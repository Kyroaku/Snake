#pragma once

#include <vector>

#include "Mesh.h"
#include "ICollidable.h"
#include "ISerializable.h"

using namespace std;

class Actor : public ICollidable, public ISerializable {
	vector<Mesh> mMeshes;

public:
	virtual ~Actor() {};

	vector<Mesh> &GetMeshes();

	virtual SerializedData Serialize();
	virtual void Deserialize(SerializedData &data);
};