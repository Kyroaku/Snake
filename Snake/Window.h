#pragma once

#include <vector>

#include "WindowOption.h"
#include "IWindowConfirmListener.h"

using std::vector;
using std::string;

class Window {
private:
	string mTitle = "";
	vector<WindowOption> mOptions;
	int8_t mSelectedOption = -1;
	IWindowConfirmListener *mWindowConfirmListener = 0;

public:
	Window(string title = "");

	void Draw();

	void AddOption(WindowOption option);

	void SetSelectedOption(int8_t i);
	void SetSelectedOptionById(int8_t id);
	void SetTitle(string title);
	void SetWindowConfirmListener(IWindowConfirmListener *listener);

	WindowOption *GetSelectedOption();
	string GetTitle();
	vector<WindowOption> &GetOptions();

	void RemoveAllOptions();

	void SelectNext();
	void SelectPrevious();
	void Confirm();
};