#include "Application.h"
#include "Settings.h"

#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
	if (argc > 1) {
		Settings::ServerIp = argv[1];
		if (argc > 2)
			Settings::ServerPort = (uint16_t)atoi(argv[2]);
	}
	Application.Start();
}