#include "NetworkEvent.h"

#include "Snake.h"

NetworkEvent::NetworkEvent(uint8_t action /* = 0xFF */, ISerializable *params /* = 0 */, size_t id /* = -1 */)
	: mAction(action)
	, mClientId(id)
	, mParams(unique_ptr<ISerializable>(params)) {
}

uint8_t NetworkEvent::GetAction() {
	return mAction;
}

size_t NetworkEvent::GetClientId() {
	return mClientId;
}

void NetworkEvent::SetClientId(size_t id) {
	mClientId = id;
}

SerializedData NetworkEvent::Serialize() {
	SerializedData data;
	data.Push(mAction);
	data.Push(mClientId);
	if (mParams)
		data.Push(mParams->Serialize());
	return data;
}

void NetworkEvent::Deserialize(SerializedData &data) {
	data.Pop(mAction);
	data.Pop(mClientId);
	switch (mAction) {
	case NetworkEvent::ePlayerInfo:			mParams = make_unique<PlayerInfoParams>();			break;
	case NetworkEvent::eConnectedPlayers:	mParams = make_unique<ConnectedPlayersParams>();	break;
	case NetworkEvent::eSnakeInfo:			mParams = make_unique<Snake>();						break;
	case NetworkEvent::eFruitsInfo:			mParams = make_unique<Actor>();						break;
	default:
		mParams = 0;
		break;
	}
	if (mParams)
		mParams->Deserialize(data);
}