#include "Mesh.h"

Mesh::Mesh()
: mPosX(0)
, mPosY(0) {

}

Pixel &Mesh::GetPixel() {
	return mPixel;
}

int Mesh::GetX() {
	return mPosX;
}

int Mesh::GetY() {
	return mPosY;
}

void Mesh::SetPosition(int x, int y) {
	mPosX = x;
	mPosY = y;
}

SerializedData Mesh::Serialize() {
	SerializedData data;
	data.Push(mPosX);
	data.Push(mPosY);
	data.Push(mPixel.mChar);
	data.Push(mPixel.mForegroundColor);
	data.Push(mPixel.mBackgroundColor);
	return data;
}
void Mesh::Deserialize(SerializedData &data) {
	data.Pop(mPosX);
	data.Pop(mPosY);
	data.Pop(mPixel.mChar);
	data.Pop(mPixel.mForegroundColor);
	data.Pop(mPixel.mBackgroundColor);
}