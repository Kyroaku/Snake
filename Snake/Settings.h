#pragma once

#include <string>

using std::string;

class Settings {
public:
	static string ServerIp;
	static uint16_t ServerPort;
};