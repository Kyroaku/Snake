#pragma once

#include <WinSock2.h>
#include <WS2tcpip.h>
#include <string>

#include "NetworkAddress.h"

using std::string;

class Socket {
public:
	enum EProtocol {
		eTcpProtocol,
		eUdpProtocol
	};

protected:
	SOCKET mSocket;
	sockaddr_in mAddrInfo;

public:
	Socket(SOCKET sock = NULL);
	Socket(EProtocol protocol);
	virtual ~Socket();

	bool Bind(NetworkAddress address);

	void SetBroadcastEnabled(bool b);
	void SetAddressReusable(bool b);

	string GetIp();
	uint16_t GetPort();
	SOCKET GetSocket();

	void SetSocket(SOCKET sock);
	void SetAddrInfo(sockaddr_in &addr);

	int SendTo(NetworkAddress address, void *data, int size);
	int ReceiveFrom(void *data, int size, NetworkAddress *address = 0);

	bool IsConnected();
	void Close();
};