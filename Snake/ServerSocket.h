#pragma once

#include <string>
#include <memory>

#include "Socket.h"
#include "ClientSocket.h"

using std::string;
using std::shared_ptr;
using std::make_shared;

class ServerSocket : public Socket {
private:
	int mMaxClients = 1;

public:
	bool OpenServer(string ip, uint16_t port);
	shared_ptr<ClientSocket> Accept();

	void SetMaxClients(int maxClients);
};