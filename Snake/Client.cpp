#include "Client.h"

Client::Client(shared_ptr<ClientSocket> socket)
	: mSocket(socket) {

}

shared_ptr<ClientSocket> Client::GetSocket() {
	return mSocket;
}

SOCKET Client::GetSocketHandle() {
	return mSocket->GetSocket();
}

string Client::GetName() {
	return mName;
}

void Client::SetName(string name) {
	mName = name;
}