#include "Snake.h"

#include "Application.h"
#include "Server.h"
#include "Console.h"

void Snake::Update() {
	if (Lifes() > 0)
		mMoveTimer += Application.GetDeltaTime();

	if (mMoveTimer >= mSpeed) {
		//TODO: uncomment //mMoveTimer -= mSpeed; 
		mMoveTimer = 0; //TODO: delete
		Move();
	}
}

void Snake::Move() {
	for (size_t i = 0; i < GetMeshes().size() - 1; i++) {
		GetMeshes().at(i).SetPosition(
			GetMeshes().at(i + 1).GetX(),
			GetMeshes().at(i + 1).GetY()
		);
	}

	Mesh &head = GetMeshes().back();

	switch (mDirection) {
	case eLeft:
		head.SetPosition(head.GetX() - 1, head.GetY());
		break;

	case eRight:
		head.SetPosition(head.GetX() + 1, head.GetY());
		break;

	case eUp:
		head.SetPosition(head.GetX(), head.GetY() - 1);
		break;

	case eDown:
		head.SetPosition(head.GetX(), head.GetY() + 1);
		break;
	}

	Server.SendToAll(NetworkEvent(NetworkEvent::eSnakeInfo, new Snake(*this), mId));
}

void Snake::Direction(Snake::EDirection direction) {
	mDirection = direction;
}

Snake::EDirection Snake::Direction() {
	return mDirection;
}

void Snake::Id(uint8_t id) {
	mId = id;
}

uint8_t Snake::Id() {
	return mId;
}

uint8_t Snake::Lifes() {
	return mNumLifes;
}

void Snake::AddLife() {
	mNumLifes++;
	Mesh mesh;
	mesh.GetPixel().mChar = ASCII::Smile;
	mesh.SetPosition(GetMeshes().back().GetX(), GetMeshes().back().GetY());
	GetMeshes().push_back(mesh);
}

void Snake::RemoveLife() {
	if (mNumLifes <= 0)
		return;

	mNumLifes--;
	GetMeshes().pop_back();
}

void Snake::AddLength() {
	Mesh mesh;
	mesh.GetPixel().mChar = 'o';
	mesh.GetPixel().mForegroundColor = Pixel::eBlack;
	mesh.SetPosition(GetMeshes().front().GetX(), GetMeshes().front().GetY());
	GetMeshes().insert(GetMeshes().begin(), mesh);
}

void Snake::Collision(ICollidable &collider, int collisionX, int collisionY) {
	collider.Collision(*this, collisionX, collisionY);
}

void Snake::Collision(Snake &snake, int collisionX, int collisionY) {
	Mesh &head = GetMeshes().back();
	if (head.GetX() == collisionX && head.GetY() == collisionY) {
		RemoveLife();
	}
}

void Snake::Collision(Tile &tile, int collisionX, int collisionY) {
	Mesh &head = GetMeshes().back();
	if (head.GetX() == collisionX && head.GetY() == collisionY) {
		RemoveLife();
	}
}

SerializedData Snake::Serialize() {
	SerializedData data;
	data.Push(__super::Serialize());
	data.Push(mDirection);
	data.Push(mSpeed);
	return data;
}
void Snake::Deserialize(SerializedData &data) {
	__super::Deserialize(data);
	data.Pop(mDirection);
	data.Pop(mSpeed);
}