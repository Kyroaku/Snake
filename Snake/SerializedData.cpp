#include "SerializedData.h"

SerializedData::SerializedData() {
	mData = new vector<uint8_t>();
}

SerializedData::SerializedData(uint8_t *data, int dataLen) {
	mData = new vector<uint8_t>(data, &data[dataLen]);
}

vector<uint8_t> *SerializedData::GetByteArray() {
	return mData;
}

size_t SerializedData::GetSize() {
	return (mWritePointer - mReadPointer);
}