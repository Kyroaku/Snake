#pragma once

#include <vector>
#include <memory>

#include "PlayerData.h"

using std::vector;
using std::shared_ptr;

#define Database (SDatabase::GetInstance())

class SDatabase {
	SDatabase();
	~SDatabase();
	SDatabase(const SDatabase&) = delete;
	void operator=(const SDatabase&) = delete;

	vector<shared_ptr<PlayerData>> mPlayers;

public:
	static SDatabase & GetInstance();

	void Clear();

	void AddPlayer(PlayerData *playerData);
	void RemovePlayer(size_t i);

	shared_ptr<PlayerData> GetPlayer(size_t id);
	vector<shared_ptr<PlayerData>> &GetPlayers();
};