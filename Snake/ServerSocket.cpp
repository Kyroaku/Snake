#include "ServerSocket.h"

bool ServerSocket::OpenServer(string ip, uint16_t port) {
	mSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (mSocket == INVALID_SOCKET) {
		return false;
	}

	mAddrInfo.sin_family = AF_INET;
	mAddrInfo.sin_port = htons(port);
	InetPton(AF_INET, ip.c_str(), &mAddrInfo.sin_addr.s_addr);

	if (bind(mSocket, (const sockaddr*)&mAddrInfo, sizeof(mAddrInfo)) == SOCKET_ERROR) {
		return false;
	}

	if (listen(mSocket, mMaxClients) == SOCKET_ERROR) {
		return false;
	}

	return true;
}

shared_ptr<ClientSocket> ServerSocket::Accept() {
	shared_ptr<ClientSocket> client = make_shared<ClientSocket>();
	SOCKET sock;
	sockaddr_in addr;
	int len = sizeof(addr);
	sock = accept(mSocket, (sockaddr*)&addr, &len);
	client->SetSocket(sock);
	client->SetAddrInfo(addr);
	return client;
}

void ServerSocket::SetMaxClients(int maxClients) {
	mMaxClients = maxClients;
}