#pragma once

#include "Actor.h"

class Snake : public Actor {
public:
	enum EDirection : uint8_t {
		eLeft, eRight, eUp, eDown
	};

private:
	EDirection mDirection = eRight;
	float mSpeed = 0.5f;
	float mMoveTimer = 0.0f;
	uint8_t mId;
	uint8_t mNumLifes = 1;

public:
	void Update();

	void Move();

	void Direction(Snake::EDirection direction);
	Snake::EDirection Direction();

	void Id(uint8_t id);
	uint8_t Id();

	uint8_t Lifes();
	void AddLife();
	void RemoveLife();

	void AddLength();

	void Collision(ICollidable &collider, int collisionX, int collisionY);
	void Collision(Snake &snake, int collisionX, int collisionY);
	void Collision(Tile &tile, int collisionX, int collisionY);

	virtual SerializedData Serialize();
	virtual void Deserialize(SerializedData &data);
};