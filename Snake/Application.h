#pragma once

#include "NetworkManager.h"
#include "BaseState.h"

#include <vector>

using std::vector;

#define Application (SApplication::Instance())


class SApplication {
private:
	bool mIsRunning = true;
	float mDeltaTime = 0.0f;

	shared_ptr<BaseState> mAppState;

	SApplication() {}
	SApplication(const SApplication&) = delete;
	void operator=(const SApplication&) = delete;
public:
	~SApplication();

	static SApplication &Instance();

	void Start();
	void Init();

	void SetState(shared_ptr<BaseState> state);

	float GetDeltaTime();

	void Quit();
};