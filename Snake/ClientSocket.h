#pragma once

#include "Socket.h"

class ClientSocket : public Socket {
public:
	ClientSocket(SOCKET sock = NULL);
	ClientSocket(string ip, uint16_t port);

	bool Connect(string ip, uint16_t port);

	bool Send(void *data, int size);
	int Receive(void *data, int buf_size, int data_size = 0);
};