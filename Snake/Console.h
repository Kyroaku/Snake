#pragma once

#include <Windows.h>
#include <string>

#include "Pixel.h"
#include "ASCII.h"

#define Console (ConsoleManager::Instance())

using namespace std;

class ConsoleManager {
private:
	HANDLE mStdHandle = NULL;
	WORD mActualColor = 0;
	short mWidth = -1;
	short mHeight = -1;

	ConsoleManager();
	ConsoleManager(const ConsoleManager&) = delete;
	void operator=(const ConsoleManager&) = delete;
public:
	static ConsoleManager &Instance();

	void Clear();

	void Print(string text);
	void Print(string text, short x, short y);
	void Print(char c);
	void Print(Pixel &p);
	void Print(char c, short x, short y);
	void Print(Pixel &p, short x, short y);

	void PrintCenter(string text, short y);

	void SetColor(Pixel::EColor color);
	void SetColor(Pixel::EColor textColor, Pixel::EColor backgroundColor);
	void SetBackgroundColor(Pixel::EColor color);

	uint8_t GetActualColor();

	void SetMinimalBufferSize();

	void SetWindowSize(short width, short height);

	void ShowCursor(bool b);

	short GetWindowWidth();
	short GetWindowHeight();
};