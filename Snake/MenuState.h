#pragma once

#include <thread>

#include "BaseState.h"
#include "Window.h"
#include "InputController.h"

using std::thread;

class MenuState : public BaseState, IWindowConfirmListener {
private:
	enum EWindowOptions {
		eDummy,
		eStartGame,
		eQuit,
		eNewGame,
		eJoinGame,
		eStartGameBack,
		eConnectionStart,
		eConnectionBack,
		eConnectedLeave,
		eSettings,
		eSettingsBack
	};

	InputController mInputController;

	shared_ptr<Window> mCurrentWindow;

	bool mRedrawNeeded = true;

public:
	shared_ptr<Window> mMainWindow;
	shared_ptr<Window> mStartGameWindow;
	shared_ptr<Window> mConnectionWindow;
	shared_ptr<Window> mConnectedWindow;
	shared_ptr<Window> mSettingsWindow;

public:
	MenuState();
	~MenuState();

	void HandleInput(int key);
	void HandleNetwork(unique_ptr<NetworkEvent> e);
	void Update();
	void Draw();

	void UpdateConnectionWindow();
	void UpdateConnectedWindow();

	void OnWindowConfirm(Window *window, int id);

	void SetCurrentWindow(shared_ptr<Window> window);

	shared_ptr<Window> GetCurrentWindow();
};