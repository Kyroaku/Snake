#include "CollisionManager.h"

void CollisionManager::HandleCollisions(Actor &actor1, Actor &actor2) {
	for (auto &mesh : actor1.GetMeshes()) {
		for (auto &mesh2 : actor2.GetMeshes()) {
			if (mesh.GetX() == mesh2.GetX() && mesh.GetY() == mesh2.GetY())
				actor2.Collision(actor1, mesh.GetX(), mesh.GetY());
		}
	}
}

void CollisionManager::HandleCollisions(Actor &actor, Map &map) {
	for (auto &mesh : actor.GetMeshes()) {
		Tile &tile = map.GetTile(mesh.GetX(), mesh.GetY());
		if (tile.mType == Tile::eCollider) {
			actor.Collision((ICollidable&)tile, mesh.GetX(), mesh.GetY());
			tile.Collision(actor, mesh.GetX(), mesh.GetY());
		}
	}
}