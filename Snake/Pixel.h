#pragma once

class Pixel {
public:
	enum EColor : unsigned char {
		eBlack = 0,
		eDarkBlue = 1,
		eDarkGreen = 2,
		eDarkCyan = 3,
		eDarkRed = 4,
		eDarkMagenta = 5,
		eDarkYellow = 6,
		eGray = 7,
		eDarkGray = 8,
		eBlue = 9,
		eGreen = 10,
		eCyan = 11,
		eRed = 12,
		eMagenta = 13,
		eYellow = 14,
		eWhite = 15
	};

public:
	char mChar;
	EColor mForegroundColor, mBackgroundColor;

public:
	Pixel(char character = 'X', EColor foregroundColor = eGray, EColor backgroundColor = eBlack);

	bool operator==(Pixel p) {
		return (p.mChar == mChar && p.mForegroundColor == mForegroundColor && p.mBackgroundColor == mBackgroundColor);
	}
	bool operator!=(Pixel p) {
		return !(p.mChar == mChar && p.mForegroundColor == mForegroundColor && p.mBackgroundColor == mBackgroundColor);
	}
	void operator=(Pixel p) {
		mChar = p.mChar;
		mForegroundColor = p.mForegroundColor;
		mBackgroundColor = p.mBackgroundColor;
	}
};