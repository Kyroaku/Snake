#pragma once

#include "Snake.h"

class Fruit : public Mesh {
public:
	Fruit();

	void OnEat(Snake &snake);
};