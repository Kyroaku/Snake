#pragma once

#include <vector>
#include <thread>
#include <future>
#include <memory>
#include <queue>
#include <mutex>

#include "NetworkPacketHeader.h"
#include "NetworkManager.h"
#include "NetworkEvent.h"

#define Server (SServer::GetInstance())

using std::vector;
using std::thread;
using std::shared_ptr;
using std::future;
using std::queue;
using std::make_shared;
using std::mutex;
using std::lock_guard;

class SServer {
public:
	vector<shared_ptr<ClientSocket>> mClients;		///< List of sockets connected to server.
	ClientSocket mClientServer;			///< Socket that local client is connected to.
	ServerSocket mServer;				///< Server socket that listens for connections.

	std::future<void> mAcceptThread;	///< Future of thread for accepting clients.

	fd_set mFdRead;						///< Set of sockets to handle by server.

	future<void> mNetworkFuture;
	future<void> mServerSearchFuture;

	mutex mEventQueueMutex;
	queue<unique_ptr<NetworkEvent>> mEventQueue;

private:
	SServer() {}
	SServer(const SServer&) = delete;
	void operator=(const SServer&) = delete;

	void StartNetworkThread();

	/**
	* Sends command to a specified socket.
	*/
	bool SendToSocket(ClientSocket &socket, NetworkEvent &e);

	/**
	* Receives command from a specified socket.
	*/
	int ReceiveFromSocket(ClientSocket &socket, NetworkEvent *e);

	void AddEvent(unique_ptr<NetworkEvent> e);

public:
	static SServer &GetInstance();

	/**
	 * Initializes and opens server.
	 */
	bool Init();
	/**
	 * Disconnects all clients and closes the server.
	 */
	void Stop();

	void StartServerSearchThread();

	/**
	 * Adds client to connected clients list.
	 */
	void AddClient(shared_ptr<ClientSocket> client);

	/**
	 * Returns number of connected clients.
	 */
	size_t GetNumClients();

	/**
	 * Connects to server with specified ip and port.
	 */
	bool Connect(string ip, uint16_t port);

	/**
	 * Opens server at specified IP address and port number.
	 */
	bool OpenServer(string ip, uint16_t port);

	/**
	 * Sends command to server.
	 */
	bool Send(NetworkEvent e);

	/**
	 * Sends command to client specified by ID.
	 */
	void SendTo(size_t id, NetworkEvent e);

	/**
	 * Sends command to all clients.
	 */
	void SendToAll(NetworkEvent e, int excludedId = -1);

	unique_ptr<NetworkEvent> GetEvent();

	bool IsEventAvailable();

	bool IsLocal();
};