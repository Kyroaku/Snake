#include "Tile.h"

Tile::Tile()
	: Tile(Pixel(' '), eGround) {
}

Tile::Tile(Pixel c, EType type)
: mPixel(c)
, mType(type) {
}

void Tile::Collision(ICollidable &collider, int collisionX, int collisionY) {
	collider.Collision(*this, collisionX, collisionY);
}