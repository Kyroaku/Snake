#include "ConsoleRenderer.h"

#include "Console.h"

ConsoleRenderer::ConsoleRenderer(short width, short height)
	: mWidth(width)
	, mHeight(height) {
	mBuffer1 = unique_ptr<Pixel[]>(new Pixel[width * height]);
	mBuffer2 = unique_ptr<Pixel[]>(new Pixel[width * height]);
	memset(mBuffer1.get(), 0, width*height);
	memset(mBuffer2.get(), 0, width*height);
}

void ConsoleRenderer::Draw(Pixel c, short x, short y) {
	mBuffer1.get()[y*mWidth + x] = c;
}

void ConsoleRenderer::EndFrame() {
	int size = mWidth * mHeight;
	for (int i = 0; i < size; i++) {
		if (mBuffer1.get()[i] != mBuffer2.get()[i])
			Console.Print(mBuffer1.get()[i], i%mWidth, i / mWidth);
	}
	swap(mBuffer1, mBuffer2);
}