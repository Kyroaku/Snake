#pragma once

#include "BaseState.h"
#include "GameState.h"
#include "MenuState.h"

using std::shared_ptr;

class ApplicationStates {
public:
	static shared_ptr<BaseState> Game;
	static shared_ptr<BaseState> MainMenu;
};