#include "ClientSocket.h"

ClientSocket::ClientSocket(SOCKET sock /* = NULL */)
	: Socket(sock) {
	if (sock) {
		int len = sizeof(mAddrInfo);
		getpeername(sock, (sockaddr*)&mAddrInfo, &len);
	}
}

ClientSocket::ClientSocket(string ip, uint16_t port)
	: ClientSocket(NULL) {
	Connect(ip, port);
}

bool ClientSocket::Connect(string ip, uint16_t port) {
	mSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (mSocket == INVALID_SOCKET) {
		return false;
	}

	mAddrInfo.sin_family = AF_INET;
	mAddrInfo.sin_port = htons(port);
	InetPton(AF_INET, ip.c_str(), &mAddrInfo.sin_addr.s_addr);

	if (connect(mSocket, (const sockaddr*)&mAddrInfo, sizeof(mAddrInfo)) == SOCKET_ERROR) {
		this->Close();
		return false;
	}

	return true;
}

bool ClientSocket::Send(void *data, int size) {
	char *buffer = (char*)data;
	int sent = 0;
	while (size > 0) {
		sent = send(mSocket, buffer, size, 0);
		if (sent == SOCKET_ERROR) {
			return false;
		}
		buffer += sent;
		size -= sent;
	}
	return true;
}

int ClientSocket::Receive(void *data, int buf_size, int data_size /* = 0 */) {
	int received = 0;
	int totalReceived = 0;
	char *buffer = (char*)data;
	do {
		received = recv(mSocket, buffer, buf_size, 0);
		if (received <= 0) {
			return received;
		}
		buffer += received;
		totalReceived += received;
	} while (data_size > totalReceived && buf_size > totalReceived);
	return totalReceived;
}