#pragma once

#include "Snake.h"

class InputCommand {
public:
	virtual void Execute(void *param = 0) = 0;
};