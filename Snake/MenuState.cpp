#include "MenuState.h"

#include <functional>

#include "Application.h"
#include "Server.h"
#include "Console.h"
#include "ApplicationStates.h"
#include "Command.h"
#include "Database.h"
#include "Settings.h"

MenuState::MenuState() {
	/* Window handling callbacks. */
	class DownCommand : public InputCommand {
	public: void Execute(void *param) {
		Window *window = (Window*)param;
		window->SelectNext();
	}
	};

	class UpCommand : public InputCommand {
	public: void Execute(void *param) {
		Window *window = (Window*)param;
		window->SelectPrevious();
	}
	};

	class ConfirmCommand : public InputCommand {
	public: void Execute(void *param) {
		Window *window = (Window*)param;
		window->Confirm();
	}
	};

	mInputController.SetDownKeyCommand(shared_ptr<InputCommand>(new DownCommand()));
	mInputController.SetUpKeyCommand(shared_ptr<InputCommand>(new UpCommand()));
	mInputController.SetConfirmKeyCommand(shared_ptr<InputCommand>(new ConfirmCommand()));

	/* Create windows. */
	mMainWindow = shared_ptr<Window>(new Window("Main Menu"));
	mMainWindow->AddOption(WindowOption("Start Game", eStartGame));
	mMainWindow->AddOption(WindowOption("Settings", eSettings));
	mMainWindow->AddOption(WindowOption("Quit", eQuit));
	mMainWindow->SetWindowConfirmListener(this);

	mSettingsWindow = shared_ptr<Window>(new Window("Settings"));
	mSettingsWindow->AddOption(WindowOption("Back", eSettingsBack));
	mSettingsWindow->SetWindowConfirmListener(this);

	mStartGameWindow = shared_ptr<Window>(new Window("Start Game"));
	mStartGameWindow->AddOption(WindowOption("New Game", eNewGame));
	mStartGameWindow->AddOption(WindowOption("Join Game", eJoinGame));
	mStartGameWindow->AddOption(WindowOption("Back", eStartGameBack));
	mStartGameWindow->SetWindowConfirmListener(this);

	mConnectionWindow = shared_ptr<Window>(new Window("Waiting for players"));
	mConnectionWindow->SetWindowConfirmListener(this);

	mConnectedWindow = shared_ptr<Window>(new Window("Connected to server"));
	mConnectedWindow->AddOption(WindowOption("Waiting for game start...", 0, WindowOption::eSmall | WindowOption::eNotSelectable));
	mConnectedWindow->AddOption(WindowOption("Connected players:", 0, WindowOption::eNotSelectable));
	mConnectedWindow->AddOption(WindowOption("Leave", eConnectedLeave));
	mConnectedWindow->SetWindowConfirmListener(this);

	SetCurrentWindow(mMainWindow);
}

MenuState::~MenuState() {

}

void MenuState::HandleInput(int key) {
	shared_ptr<InputCommand> command = mInputController.HandleInput(key);
	if (command) {
		command->Execute(mCurrentWindow.get());
	}

	mRedrawNeeded = true;
}

void MenuState::HandleNetwork(unique_ptr<NetworkEvent> e) {
	switch (e->GetAction()) {

		/* ================ To server =============== */

	case NetworkEvent::eClientAccepted: {
		Database.AddPlayer(new PlayerData());
		UpdateConnectionWindow();
		Server.SendTo(e->GetClientId(), NetworkEvent(NetworkEvent::eGetPlayerInfo));
	} break;

	case NetworkEvent::eClientDisconnected: {
		Database.RemovePlayer(e->GetClientId());
		UpdateConnectionWindow();
		ConnectedPlayersParams *players = new ConnectedPlayersParams();
		for (auto player : Database.GetPlayers()) {
			PlayerInfoParams playerInfo;
			playerInfo.mName = player->mName;
			players->mPlayers.push_back(playerInfo);
		}
		Server.SendToAll(NetworkEvent(NetworkEvent::eConnectedPlayers, players));
	} break;

	case NetworkEvent::ePlayerInfo: {
		PlayerInfoParams *params = e->GetParams<PlayerInfoParams>();
		Database.GetPlayer(e->GetClientId())->mName = params->mName;
		
		ConnectedPlayersParams *players = new ConnectedPlayersParams();
		for (auto player : Database.GetPlayers()) {
			PlayerInfoParams playerInfo;
			playerInfo.mName = player->mName;
			players->mPlayers.push_back(playerInfo);
		}
		Server.SendToAll(NetworkEvent(NetworkEvent::eConnectedPlayers, players));

		UpdateConnectionWindow();
	} break;

		/* ================ To client =============== */

	case NetworkEvent::eGetPlayerInfo: {
		PlayerInfoParams *params = new PlayerInfoParams();
		params->mName = "Player " + to_string(rand() % 100);
		Server.Send(NetworkEvent(NetworkEvent::ePlayerInfo, params));
	} break;

	case NetworkEvent::eConnectedPlayers: {
		if (Server.IsLocal())
			break;

		Database.GetPlayers().clear();
		ConnectedPlayersParams *params = e->GetParams<ConnectedPlayersParams>();
		for (auto player : params->mPlayers) {
			PlayerData *data = new PlayerData();
			data->mName = player.mName;
			Database.AddPlayer(data);
		}
		UpdateConnectedWindow();
	} break;

		/* ================= To both ================ */

	case NetworkEvent::eServerDisconnected: {
		if (mCurrentWindow == mConnectionWindow) {
			UpdateConnectionWindow();
		}
	} break;

	case NetworkEvent::eStartGame:
		Console.Clear();
		Application.SetState(make_shared<GameState>());
		break;

	case NetworkEvent::eServerFound:
		if (mCurrentWindow == mConnectedWindow || Server.IsLocal())
			break;

		NetworkAddress *address = e->GetParams<NetworkAddress>();
		Settings::ServerIp = address->mIp;
		if (Server.Init())
			if (Server.Connect(Settings::ServerIp, Settings::ServerPort)) {
				Console.Clear();
				SetCurrentWindow(mConnectedWindow);
			}
		break;

	}
}

void MenuState::Update() {

}

void MenuState::Draw() {
	if (!mRedrawNeeded)
		return;

	Console.Clear();
	mCurrentWindow->Draw();

	mRedrawNeeded = false;
}

void MenuState::OnWindowConfirm(Window *window, int id) {
	switch (id) {
		/* ================================= Main window */
	case eStartGame:
		SetCurrentWindow(mStartGameWindow);
		break; 

	case eSettings:
		SetCurrentWindow(mSettingsWindow);
		break;

	case eQuit:
		Application.Quit();
		break;

		/* ================================ Start Game window */
	case eNewGame: {
		if (Server.Init()) {
			if (Server.OpenServer(Settings::ServerIp, Settings::ServerPort)) {
				Server.Connect(Settings::ServerIp, Settings::ServerPort);
				Server.StartServerSearchThread();
				SetCurrentWindow(mConnectionWindow);
			}
			else
				Server.Stop();
		}
		else
			Server.Stop();
	} break;

	case eJoinGame: {
		Server.StartServerSearchThread();
	} break;

	case eStartGameBack:
		SetCurrentWindow(mMainWindow);
		break;

		/* ================================ Settings window */
	case eSettingsBack:
		SetCurrentWindow(mMainWindow);
		break;

		/* ================================ Connection window */
	case eConnectionStart:
		Console.Clear();
		Server.SendToAll(NetworkEvent(NetworkEvent::eStartGame));
		break;

	case eConnectionBack:
		Server.Stop();
		Database.Clear();
		SetCurrentWindow(mStartGameWindow);
		break;

		/* ================================ Waiting for owner window */
	case eConnectedLeave:
		Server.Stop();
		Database.Clear();
		SetCurrentWindow(mStartGameWindow);
		break;
	}
}

void MenuState::UpdateConnectionWindow() {
	int id = 0;
	WindowOption *selected = mConnectionWindow->GetSelectedOption();
	if (selected)
		id = selected->mId;

	mConnectionWindow->RemoveAllOptions();
	mConnectionWindow->AddOption(WindowOption("127.0.0.1:1440", 0, WindowOption::eNotSelectable));
	mConnectionWindow->AddOption(WindowOption("Connected players:", 0, WindowOption::eNotSelectable));
	int i = 1;
	for (auto client : Database.GetPlayers()) {
		mConnectionWindow->AddOption(WindowOption(to_string(i) + ". " + client->mName, 0, WindowOption::eSmall | WindowOption::eNotSelectable));
		i++;
	}
	mConnectionWindow->AddOption(WindowOption("Start Game", eConnectionStart));
	mConnectionWindow->AddOption(WindowOption("Back", eConnectionBack));

	if (id == 0)
		mConnectionWindow->SetSelectedOption(0);
	else
		mConnectionWindow->SetSelectedOptionById(id);

	mRedrawNeeded = true;
}

void MenuState::UpdateConnectedWindow() {
	int id = 0;
	WindowOption *selected = mConnectedWindow->GetSelectedOption();
	if (selected)
		id = selected->mId;

	mConnectedWindow->RemoveAllOptions();
	mConnectedWindow->AddOption(WindowOption("Waiting for game start...", 0, WindowOption::eSmall | WindowOption::eNotSelectable));
	mConnectedWindow->AddOption(WindowOption("Connected players:", 0, WindowOption::eNotSelectable));
	int i = 1;
	for (auto client : Database.GetPlayers()) {
		mConnectedWindow->AddOption(WindowOption(to_string(i) + ". " + client->mName, 0, WindowOption::eSmall | WindowOption::eNotSelectable));
		i++;
	}
	mConnectedWindow->AddOption(WindowOption("Leave", eConnectedLeave));

	if (id == 0)
		mConnectedWindow->SetSelectedOption(0);
	else
		mConnectedWindow->SetSelectedOptionById(id);

	mRedrawNeeded = true;
}

void MenuState::SetCurrentWindow(shared_ptr<Window> window) {
	mCurrentWindow = window;
	mCurrentWindow->SetSelectedOption(0);
}

shared_ptr<Window> MenuState::GetCurrentWindow() {
	return mCurrentWindow;
}