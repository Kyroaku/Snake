#include "Console.h"

#include <iostream>

ConsoleManager::ConsoleManager() {
	mStdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(mStdHandle, &csbi);
	mWidth = csbi.srWindow.Right - csbi.srWindow.Left;
	mHeight = csbi.srWindow.Bottom - csbi.srWindow.Top;
}

ConsoleManager &ConsoleManager::Instance() {
	static ConsoleManager console;
	return console;
}

void ConsoleManager::Clear() {
	DWORD unused;
	FillConsoleOutputCharacter(mStdHandle, ' ', mWidth*mHeight, COORD{ 0, 0 }, &unused);
	COORD coord = { 0, 0 };
	SetConsoleCursorPosition(mStdHandle, coord);
}

void ConsoleManager::Print(string text) {
	cout << text;
}

void ConsoleManager::Print(string text, short x, short y) {
	COORD coord = { x, y };
	SetConsoleCursorPosition(mStdHandle, coord);
	cout << text;
}

void ConsoleManager::Print(char c) {
	putchar(c);
}

void ConsoleManager::Print(Pixel &p) {
	SetColor(p.mForegroundColor, p.mBackgroundColor);
	Print(p.mChar);
}

void ConsoleManager::Print(char c, short x, short y) {
	COORD coord = { x, y };
	SetConsoleCursorPosition(mStdHandle, coord);
	putchar(c);
}

void ConsoleManager::Print(Pixel &p, short x, short y) {
	SetColor(p.mForegroundColor, p.mBackgroundColor);
	Print(p.mChar, x, y);
}

void ConsoleManager::PrintCenter(string text, short y) {
	Print(text, (mWidth - (short)text.length()) / 2, y);
}

void ConsoleManager::SetColor(Pixel::EColor color) {
	mActualColor = (mActualColor & 0xF0) | (color & 0x0F);
	SetConsoleTextAttribute(mStdHandle, mActualColor);
}

void ConsoleManager::SetColor(Pixel::EColor textColor, Pixel::EColor backgroundColor) {
	SetColor(textColor);
	SetBackgroundColor(backgroundColor);
}

void ConsoleManager::SetBackgroundColor(Pixel::EColor color) {
	mActualColor = (mActualColor & 0x0F) | ((color << 4) & 0xF0);
	SetConsoleTextAttribute(mStdHandle, mActualColor);
}

uint8_t ConsoleManager::GetActualColor() {
	return (uint8_t)mActualColor;
}

void ConsoleManager::SetMinimalBufferSize() {
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(mStdHandle, &csbi);

	COORD coord;
	coord.X = csbi.srWindow.Right - csbi.srWindow.Left + 1;
	coord.Y = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
	SetConsoleScreenBufferSize(mStdHandle, coord);
}

void ConsoleManager::SetWindowSize(short width, short height) {
	SMALL_RECT rect;
	rect.Left = 0;
	rect.Right = width - 1;
	rect.Top = 0;
	rect.Bottom = height - 1;
	SetConsoleWindowInfo(mStdHandle, TRUE, &rect);

	mWidth = width;
	mHeight = height;
}

void ConsoleManager::ShowCursor(bool b) {
	CONSOLE_CURSOR_INFO     cci;
	GetConsoleCursorInfo(mStdHandle, &cci);
	cci.bVisible = b;
	SetConsoleCursorInfo(mStdHandle, &cci);
}

short ConsoleManager::GetWindowWidth() {
	return mWidth;
}

short ConsoleManager::GetWindowHeight() {
	return mHeight;
}