#pragma once

#include <memory>

#include "NetworkEvent.h"

using std::shared_ptr;

class BaseState {
public:
	static shared_ptr<BaseState> Game;

	virtual void HandleInput(int key) = 0;
	virtual void HandleNetwork(unique_ptr<NetworkEvent> e) = 0;
	virtual void Update() = 0;
	virtual void Draw() = 0;
};