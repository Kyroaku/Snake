#pragma once

#include <memory>
#include "Pixel.h"

using std::unique_ptr;
using std::make_unique;

class ConsoleRenderer {
	unique_ptr<Pixel[]> mBuffer1;
	unique_ptr<Pixel[]> mBuffer2;

	short mWidth = -1;
	short mHeight = -1;

public:
	ConsoleRenderer(short width, short height);

	void Draw(Pixel c, short x, short y);

	void EndFrame();
};