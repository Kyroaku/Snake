#pragma once

#include "Actor.h"
#include "Map.h"

class CollisionManager {
public:
	static void HandleCollisions(Actor &actor1, Actor &actor2);
	static void HandleCollisions(Actor &actor, Map &map);
};