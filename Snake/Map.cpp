#include "Map.h"

#include "Console.h"

void Map::GenerateDefault(int width, int height) {
	mWidth = width;
	mHeight = height;

	mMap = Map2D(new Map1D[width]);
	for (int i = 0; i < width; i++) {
		mMap[i] = Map1D(new Tile[height]);
	}

	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {
			if (x == 0 && y == 0)						mMap[x][y] = Tile { Pixel(ASCII::LeftTopCorner2, Pixel::eBlack, Pixel::eDarkGreen), Tile::eCollider };
			else if (x == 0 && y == height - 1)			mMap[x][y] = Tile { Pixel(ASCII::LeftBottomCorner2, Pixel::eBlack, Pixel::eDarkGreen), Tile::eCollider };
			else if (x == width - 1 && y == height - 1)	mMap[x][y] = Tile { Pixel(ASCII::RightBottomCorner2, Pixel::eBlack, Pixel::eDarkGreen), Tile::eCollider };
			else if (x == width - 1 && y == 0)			mMap[x][y] = Tile { Pixel(ASCII::RightTopCorner2, Pixel::eBlack, Pixel::eDarkGreen), Tile::eCollider };
			else if (x == 0 || x == width - 1)			mMap[x][y] = Tile { Pixel(ASCII::VerticalLine2, Pixel::eBlack, Pixel::eDarkGreen), Tile::eCollider };
			else if (y == 0 || y == height - 1)			mMap[x][y] = Tile { Pixel(ASCII::HorizontalLine2, Pixel::eBlack, Pixel::eDarkGreen), Tile::eCollider };
			else										mMap[x][y] = Tile { Pixel(' ', Pixel::eBlack, Pixel::eDarkGreen), Tile::eGround };
		}
	}
}

Tile &Map::GetTile(int x, int y) {
	return mMap[x][y];
}

void Map::AddActor(shared_ptr<Actor> actor) {
	mActors.push_back(actor);
}

vector<shared_ptr<Actor>> &Map::GetActors() {
	return mActors;
}

int Map::GetWidth() {
	return mWidth;
}
int Map::GetHeight() {
	return mHeight;
}

void Map::Draw(ConsoleRenderer *renderer) {
	for (int y = 0; y < mHeight; y++) {
		for (int x = 0; x < mWidth; x++) {
			renderer->Draw(mMap[x][y].mPixel, x, y);
		}
	}

	for (size_t i = 0; i < mActors.size(); i++) {
		for (auto &mesh : mActors.at(i)->GetMeshes()) {
			mesh.GetPixel().mBackgroundColor = mMap[mesh.GetX()][mesh.GetY()].mPixel.mBackgroundColor;
			renderer->Draw(mesh.GetPixel(), mesh.GetX(), mesh.GetY());
		}
	}
}