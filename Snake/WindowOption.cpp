#include "WindowOption.h"

bool WindowOption::HasFlagSet(EFlags flag) {
	return ((mFlags & flag) != 0);
}