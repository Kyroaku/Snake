#include "Actor.h"

#include "Console.h"

vector<Mesh> &Actor::GetMeshes() {
	return mMeshes;
}

SerializedData Actor::Serialize() {
	SerializedData data;
	uint8_t len = (uint8_t)mMeshes.size();
	data.Push(len);
	for (auto &mesh : mMeshes) {
		data.Push(mesh.Serialize());
	}
	return data;
}
void Actor::Deserialize(SerializedData &data) {
	uint8_t len;
	data.Pop(len);
	mMeshes.resize(len);
	for (auto &mesh : mMeshes) {
		mesh.Deserialize(data);
	}
}