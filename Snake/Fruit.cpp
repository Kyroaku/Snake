#include "Fruit.h"

#include "ASCII.h"

Fruit::Fruit() {
	GetPixel().mChar = ASCII::Sun;
	GetPixel().mForegroundColor = Pixel::eYellow;
}

void Fruit::OnEat(Snake &snake) {
	snake.AddLength();
}