#include "InputCommands.h"

#include "Server.h"

void LeftCommand::Execute(void *param /* = 0 */) {
	Server.Send(NetworkEvent(NetworkEvent::eLeft));
}

void RightCommand::Execute(void *param /* = 0 */) {
	Server.Send(NetworkEvent(NetworkEvent::eRight));
}

void UpCommand::Execute(void *param /* = 0 */) {
	Server.Send(NetworkEvent(NetworkEvent::eUp));
}

void DownCommand::Execute(void *param /* = 0 */) {
	Server.Send(NetworkEvent(NetworkEvent::eDown));
}