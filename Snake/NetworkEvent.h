#pragma once

#include <cstdint>
#include <memory>

#include "NetworkEventParams.h"
#include "ISerializable.h"

using std::unique_ptr;
using std::make_unique;
using std::shared_ptr;
using std::move;

class NetworkEvent : public ISerializable {
public:
	enum ECommand : uint8_t {
		eClientAccepted,
		eClientDisconnected,
		eServerDisconnected,
		eGetPlayerInfo,
		ePlayerInfo,
		eConnectedPlayers,
		eStartGame,
		eLeft,
		eRight,
		eUp,
		eDown,
		eSnakeInfo,
		eFruitsInfo,
		eServerFound,

		eReserved = 0xFF
	};

private:
	uint8_t mAction;
	size_t mClientId = -1;
	unique_ptr<ISerializable> mParams;

public:
	NetworkEvent(uint8_t action = 0xFF, ISerializable *params = 0, size_t id = -1);

	uint8_t GetAction();
	size_t GetClientId();

	void SetClientId(size_t id);

	template<typename T> T* GetParams() {
		return (T*)mParams.get();
	}

	virtual SerializedData Serialize();
	virtual void Deserialize(SerializedData &data);
};