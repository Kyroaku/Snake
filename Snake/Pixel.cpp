#include "Pixel.h"

Pixel::Pixel(char character, EColor foregroundColor, EColor backgroundColor)
: mChar(character)
, mForegroundColor(foregroundColor)
, mBackgroundColor(backgroundColor) {

}