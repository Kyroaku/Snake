#pragma once

#include <string>

#include "ISerializable.h"

class PlayerInfoParams : public ISerializable {
public:
	std::string mName;

	virtual SerializedData Serialize() {
		SerializedData data;
		data.Push(mName);
		return data;
	}

	virtual void Deserialize(SerializedData &data) {
		data.Pop(mName);
	}
};

class ConnectedPlayersParams : public ISerializable {
public:
	std::vector<PlayerInfoParams> mPlayers;

	virtual SerializedData Serialize() {
		SerializedData data;
		size_t len = mPlayers.size();
		data.Push(len);
		for (auto &player : mPlayers) {
			SerializedData playerData = player.Serialize();
			data.Push(playerData);
		}
		return data;
	}

	virtual void Deserialize(SerializedData &data) {
		size_t len;
		data.Pop(len);
		mPlayers.resize(len);
		for (auto &player : mPlayers)
			player.Deserialize(data);
	}
};