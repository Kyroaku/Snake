#pragma once

#include <string>

#include "ISerializable.h"

using std::string;

class NetworkAddress : public ISerializable {
public:
	string mIp;
	uint16_t mPort;

	NetworkAddress(string ip = "0.0.0.0", uint16_t port = 0);

	SerializedData Serialize() { return SerializedData(); }
	void Deserialize(SerializedData &data) {};
};