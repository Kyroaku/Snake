#pragma once

class ASCII {
public:
	static const char Smile = (char)1;
	static const char FilledSmile = (char)2;

	static const char Sun = (char)15;

	static const char HorizontalLine = (char)196;
	static const char VerticalLine = (char)179;
	static const char LeftTopCorner = (char)218;
	static const char RightTopCorner = (char)191;
	static const char RightBottomCorner = (char)217;
	static const char LeftBottomCorner = (char)192;
	static const char CrossLine = (char)197;
	static const char LeftCrossLine = (char)180;
	static const char RightCrossLine = (char)195;
	static const char UpCrossLine = (char)193;
	static const char DownCrossLine = (char)194;

	static const char HorizontalLine2 = (char)205;
	static const char VerticalLine2 = (char)186;
	static const char LeftTopCorner2 = (char)201;
	static const char RightTopCorner2 = (char)187;
	static const char RightBottomCorner2 = (char)188;
	static const char LeftBottomCorner2 = (char)200;
	static const char CrossLine2 = (char)206;
	static const char LeftCrossLine2 = (char)185;
	static const char RightCrossLine2 = (char)204;
	static const char UpCrossLine2 = (char)202;
	static const char DownCrossLine2 = (char)203;

	static const char LowFilled = (char)176;
	static const char MediumFilled = (char)177;
	static const char HighFilled = (char)178;
	static const char FullFilled = (char)219;

	static const char LeftFilled = (char)221;
	static const char RightFilled = (char)222;
	static const char TopFilled = (char)223;
	static const char BottomFilled = (char)220;
	static const char CenterFilled = (char)254;

	//static const char Function = (char)159;
	//static const char MuchLess = (char)174;
	//static const char MuchMore = (char)175;
	//static const char OneHalf = (char)171;
	//static const char OneQuarter = (char)172;
	//static const char Alpha = (char)224;
	//static const char Beta = (char)225;
	//static const char Gamma = (char)226;
	//static const char Pi = (char)227;
	//static const char Sum = (char)228;
	//static const char Sigma = (char)229;
	//static const char Mu = (char)230;
	//static const char Tau = (char)231;
	//static const char Phi = (char)232;
	//static const char Theta = (char)233;
	//static const char Omega = (char)234;
	//static const char Delta = (char)235;
	//static const char Infinity = (char)236;
	//static const char PhiSmall = (char)237;
	//static const char Epsilon = (char)238;
	//static const char Equivalence = (char)240;
	//static const char PlusMinus = (char)241;
	//static const char MoreEqual = (char)242;
	//static const char LessEqual = (char)243;
	//static const char Integral1 = (char)244;
	//static const char Integral2 = (char)245;
	//static const char Degree = (char)248;
	//static const char Element = (char)251;
};