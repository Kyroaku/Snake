#include "GameState.h"

#include "CollisionManager.h"

#include "Server.h"
#include "Database.h"
#include "Console.h"

GameState::GameState() {
	mRenderer = new ConsoleRenderer(Console.GetWindowWidth(), Console.GetWindowHeight());

	/* Initialize local controller. */
	mInputController.SetLeftKeyCommand(shared_ptr<LeftCommand>(new LeftCommand()));
	mInputController.SetRightKeyCommand(shared_ptr<RightCommand>(new RightCommand()));
	mInputController.SetUpKeyCommand(shared_ptr<UpCommand>(new UpCommand()));
	mInputController.SetDownKeyCommand(shared_ptr<DownCommand>(new DownCommand()));

	mMap.GenerateDefault(20, 20);

	mFruitSpawner = make_shared<FruitSpawner>(&mMap);
	mMap.AddActor(mFruitSpawner);

	/* Initialize snakes. */
	int id = 0;
	for (auto player : Database.GetPlayers()) {
		shared_ptr<Snake> snake = make_shared<Snake>();
		Mesh mesh;
		for (int i = 0; i < 5; i++) {
			if (i == 4)	mesh.GetPixel().mChar = 2;
			else		mesh.GetPixel().mChar = 'o';
			switch (id) {
			case 0:
				mesh.SetPosition(i + 2, 2);
				snake->Direction(Snake::eRight);
				break;
			case 1:
				mesh.SetPosition(17, i + 2);
				snake->Direction(Snake::eDown);
				break;
			case 2:
				mesh.SetPosition(17 - i, 17);
				snake->Direction(Snake::eLeft);
				break;
			case 3:
				mesh.SetPosition(2, 17 - i);
				snake->Direction(Snake::eUp);
				break;
			}
			mesh.GetPixel().mForegroundColor = Pixel::eBlack;
			snake->GetMeshes().push_back(mesh);
		}
		snake->Id(id);
		mSnakes.push_back(snake);
		mMap.AddActor(snake);
		id++;
	}

	if (Server.IsLocal()) {
		mFruitSpawner->SpawnFruit();
	}

	Console.Clear();
}

GameState::~GameState() {

}

void GameState::HandleInput(int key) {
	/* Get and execute input command. */
	shared_ptr<InputCommand> command = mInputController.HandleInput(key);
	if (command) {
		command->Execute(0);
	}
}

void GameState::HandleNetwork(unique_ptr<NetworkEvent> e) {
	switch (e->GetAction()) {
	case NetworkEvent::eLeft:
		mSnakes[e->GetClientId()]->Direction(Snake::eLeft);
		break;

	case NetworkEvent::eRight:
		mSnakes[e->GetClientId()]->Direction(Snake::eRight);
		break;

	case NetworkEvent::eUp:
		mSnakes[e->GetClientId()]->Direction(Snake::eUp);
		break;

	case NetworkEvent::eDown:
		mSnakes[e->GetClientId()]->Direction(Snake::eDown);
		break;

	case NetworkEvent::eSnakeInfo: {
		if (Server.IsLocal())
			break;

		Snake *snake = e->GetParams<Snake>();
		mSnakes[e->GetClientId()]->GetMeshes().clear();
		mSnakes[e->GetClientId()]->GetMeshes().insert(mSnakes[e->GetClientId()]->GetMeshes().begin(), snake->GetMeshes().begin(), snake->GetMeshes().end());
	} break;

	case NetworkEvent::eFruitsInfo: {
		if (Server.IsLocal())
			break;

		Actor *fruits = e->GetParams<Actor>();
		mFruitSpawner->GetMeshes().clear();
		mFruitSpawner->GetMeshes().insert(mFruitSpawner->GetMeshes().begin(), fruits->GetMeshes().begin(), fruits->GetMeshes().end());
	} break;
	}
}

void GameState::Update() { 
	if (!Server.IsLocal())
		return;

	for (auto snake : mSnakes) {
		snake->Update();
	}

	mFruitSpawner->Update();

	for (auto actor : mMap.GetActors()) {
		for (auto actor2 : mMap.GetActors()) {
			if (actor == actor2)
				continue;

			CollisionManager::HandleCollisions(*actor, *actor2);
		}

		CollisionManager::HandleCollisions(*actor, mMap);
	}
}

void GameState::Draw() {
	mMap.Draw(mRenderer);

	mRenderer->EndFrame();
}