#include "Application.h"

#include <chrono>
#include <thread>
#include <conio.h>
#include <memory>

#include "Console.h"
#include "CollisionManager.h"
#include "ApplicationStates.h"
#include "Server.h"

#define GAME_REFRESH_RATE	(20.0)
#define GAME_REFRESH_DELAY	(1.0/GAME_REFRESH_RATE)

using namespace std;
using namespace std::chrono;

SApplication::~SApplication() {
	Server.Stop();
}

SApplication &SApplication::Instance() {
	static SApplication game;
	return game;
}

void SApplication::Start() {
	Init();

	while (mIsRunning) {
		auto start_time = system_clock::now();

		while (_kbhit()) {
			int byte1 = _getch();
			int byte2 = _getch();
			int key = (byte1 & 0xFF) | ((byte2 & 0xFF) << 8);
			mAppState->HandleInput(key);

		}
		while (Server.IsEventAvailable()) {
			mAppState->HandleNetwork(Server.GetEvent());
		}
		mAppState->Update();
		mAppState->Draw();

		mDeltaTime = duration<float>(system_clock::now() - start_time).count();
		this_thread::sleep_for(duration<float>(GAME_REFRESH_DELAY - mDeltaTime));
		mDeltaTime = duration<float>(system_clock::now() - start_time).count();
	}
}

void SApplication::Init() {
	srand((unsigned int)time(0));

	Console.SetWindowSize(50, 30);
	Console.SetMinimalBufferSize();
	Console.ShowCursor(false);

	Network.Init();

	SetState(make_shared<MenuState>());
}

void SApplication::SetState(shared_ptr<BaseState> state) {
	mAppState = shared_ptr<BaseState>(state);
}

float SApplication::GetDeltaTime() {
	return mDeltaTime;
}

void SApplication::Quit() {
	mIsRunning = false;
}