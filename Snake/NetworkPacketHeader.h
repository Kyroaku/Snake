#pragma once

#include "ISerializable.h"

#define NETWORK_PACKET_HEADER_SIZE		8

class NetworkPacketHeader : public ISerializable {
public:
	uint32_t mValidationCode = 0xDEADBEAF;
	uint32_t mSize = 0;

	NetworkPacketHeader() = default;
	NetworkPacketHeader(uint32_t size)
		: mSize(size) {
	}

	bool IsValid() {
		return (mValidationCode == 0xDEADBEAF);
	}

	SerializedData Serialize() {
		SerializedData data;
		data.Push(mValidationCode);
		data.Push(mSize);
		return data;
	}

	void Deserialize(SerializedData &data) {
		data.Pop(mValidationCode);
		data.Pop(mSize);
	}
};