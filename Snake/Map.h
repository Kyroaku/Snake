#pragma once

#include <memory>
#include <vector>

#include "Tile.h"
#include "Actor.h"

#include "ConsoleRenderer.h"

using namespace std;

class Map {
	typedef unique_ptr<Tile[]> Map1D;
	typedef unique_ptr<Map1D[]> Map2D;

	Map2D mMap;
	vector<shared_ptr<Actor>> mActors;
	int mWidth, mHeight;

public:
	void GenerateDefault(int width, int height);

	Tile &GetTile(int x, int y);

	void AddActor(shared_ptr<Actor> actor);
	vector<shared_ptr<Actor>> &GetActors();

	int GetWidth();
	int GetHeight();

	void Draw(ConsoleRenderer *renderer);
};