#pragma once

#include "Actor.h"
#include "Map.h"
#include "Fruit.h"

class FruitSpawner : public Actor {
	Map *mMap;
	vector<unique_ptr<Fruit>> mFruits;

	bool mSendRequest = false;

public:
	FruitSpawner(Map *map);

	void Update();

	void RemoveFruit(size_t i);
	void SpawnFruit();

	void SendRequest(bool b);
	bool SendRequest();

	virtual void Collision(ICollidable &, int collisionX, int collisionY);
	virtual void Collision(Snake &, int collisionX, int collisionY);
};