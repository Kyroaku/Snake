#pragma once

#include "ICollidable.h"
#include "Pixel.h"

class Tile : public ICollidable {
public:
	enum EType {
		eGround, eCollider
	};

	Tile();
	Tile(Pixel c, EType type);

	Pixel mPixel;
	EType mType;

	void Collision(ICollidable &collider, int collisionX, int collisionY);
};