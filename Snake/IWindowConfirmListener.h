#pragma once

class Window;

class IWindowConfirmListener {
public:
	virtual void OnWindowConfirm(Window *window, int id) = 0;
};