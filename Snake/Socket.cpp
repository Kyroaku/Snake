#include "Socket.h"

Socket::Socket(SOCKET sock /* = NULL */)
	: mSocket(sock) {
}

Socket::Socket(EProtocol protocol) {
	switch (protocol) {
	case eTcpProtocol:
		mSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		break;

	case eUdpProtocol:
		mSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		break;
	}
}

Socket::~Socket() {
	Close();
}

bool Socket::Bind(NetworkAddress address) {
	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(address.mPort);
	InetPton(addr.sin_family, address.mIp.c_str(), &addr.sin_addr.s_addr);

	if (bind(mSocket, (sockaddr*)&addr, sizeof(addr)) == SOCKET_ERROR)
		return false;
	else
		return true;
}

void Socket::SetBroadcastEnabled(bool b) {
	int val = b ? 1 : 0;
	setsockopt(mSocket, SOL_SOCKET, SO_BROADCAST, (const char*)&val, sizeof(val));
}

void Socket::SetAddressReusable(bool b) {
	int val = b ? 1 : 0;
	setsockopt(mSocket, SOL_SOCKET, SO_REUSEADDR, (const char*)&val, sizeof(val));
}

string Socket::GetIp() {
	char buff[16];
	InetNtop(mAddrInfo.sin_family, &mAddrInfo.sin_addr, buff, sizeof(buff));
	return string(buff);
}

uint16_t Socket::GetPort() {
	return ntohs(mAddrInfo.sin_port);
}

SOCKET Socket::GetSocket() {
	return mSocket;
}

void Socket::SetSocket(SOCKET sock) {
	mSocket = sock;
}

void Socket::SetAddrInfo(sockaddr_in &addr) {
	mAddrInfo = addr;
}

int Socket::SendTo(NetworkAddress address, void *data, int size) {
	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(address.mPort);
	InetPton(addr.sin_family, address.mIp.c_str(), &addr.sin_addr.s_addr);

	return sendto(mSocket, (char*)data, size, 0, (sockaddr*)&addr, sizeof(addr));
}

int Socket::ReceiveFrom(void *data, int size, NetworkAddress *address /* = 0 */) {
	sockaddr_in addr;
	int addrLen = sizeof(addr);
	int recvLen = recvfrom(mSocket, (char*)data, size, 0, (sockaddr*)&addr, &addrLen);
	if (address) {
		char ipbuff[16];
		InetNtop(addr.sin_family, &addr.sin_addr, ipbuff, sizeof(ipbuff));
		address->mIp = string(ipbuff);
		address->mPort = ntohs(addr.sin_port);
	}
	return recvLen;
}

bool Socket::IsConnected() {
	return (mSocket != SOCKET_ERROR && mSocket != INVALID_SOCKET && mSocket != NULL);
}

void Socket::Close() {
	if (mSocket) {
		shutdown(mSocket, SD_BOTH);
		closesocket(mSocket);
		mSocket = NULL;
	}
}