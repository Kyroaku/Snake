#include "Server.h"

#define RECEIVE_BUFFER_SIZE		4096

using namespace std;

SServer &SServer::GetInstance() {
	static SServer instance;
	return instance;
}

bool SServer::Init() {
	FD_ZERO(&mFdRead);
	return true;
}

void SServer::Stop() {
	for (auto client : mClients)
		client->Close();
	mClients.clear();
	mServer.Close();
	mClientServer.Close();
	FD_ZERO(&mFdRead);
}

void SServer::StartServerSearchThread() {
	if (mServerSearchFuture.valid())
		if (mServerSearchFuture.wait_for(0s) != future_status::ready)
			return;

	mServerSearchFuture = async(launch::async, [this]() {
		Socket sock(Socket::eUdpProtocol);
		sock.SetAddressReusable(true);
		sock.SetBroadcastEnabled(true);
		sock.Bind(NetworkAddress("0.0.0.0", 1441));
		char buffer[32];
		char validCode[] = "iamserver";

		while (1) {
			fd_set fd_read;
			FD_ZERO(&fd_read);
			FD_SET(sock.GetSocket(), &fd_read);
			timeval timeout;
			timeout.tv_sec = 1;
			timeout.tv_usec = 0;
			int result = select(0, &fd_read, 0, 0, &timeout);

			if (result == 0)
				this_thread::sleep_for(1s);
			else if (result < 0)
				break;

			if (FD_ISSET(sock.GetSocket(), &fd_read)) {
				NetworkAddress *address = new NetworkAddress();
				sock.ReceiveFrom(buffer, sizeof(buffer), address);
				if (strcmp(buffer, validCode) == 0)
					AddEvent(make_unique<NetworkEvent>(NetworkEvent::eServerFound, address));
			}

			if (IsLocal()) {
				sock.SendTo(NetworkAddress("255.255.255.255", 1441), validCode, sizeof(validCode));
				this_thread::sleep_for(1s);
			}
		}
	});
}

void SServer::StartNetworkThread() {
	if (mNetworkFuture.valid())
		if (mNetworkFuture.wait_for(0s) != future_status::ready)
			return;

	mNetworkFuture = async(launch::async, [this]() {
		while (1) {
			fd_set fd_read = mFdRead;
			/* Wait for network event. */
			int result = select(0, &fd_read, 0, 0, 0);

			if (result == SOCKET_ERROR) {
				int error = WSAGetLastError();
				if (error == 10022) {
					// Probably main socket closed (mServer or mClientServer)
					break;
				}
				this_thread::sleep_for(10ms);
			}

			/* Handle accepting connections. */
			if (mServer.IsConnected())
				if (FD_ISSET(mServer.GetSocket(), &fd_read)) {
					/* Accept connection. */
					shared_ptr<ClientSocket> client = mServer.Accept();
					if (client->IsConnected())
						mClients.push_back(client);
					FD_SET(client->GetSocket(), &mFdRead);
					AddEvent(make_unique<NetworkEvent>(NetworkEvent::eClientAccepted, nullptr, mClients.size() - 1));
				}

			/* Handle client events. */
			for (size_t i = 0; i < mClients.size(); i++) {
				if (FD_ISSET(mClients.at(i)->GetSocket(), &fd_read)) {
					unique_ptr<NetworkEvent> e = make_unique<NetworkEvent>();
					int dataLen = ReceiveFromSocket(*mClients.at(i), e.get());
					if (dataLen > 0) {
						e->SetClientId(i);
						AddEvent(move(e));
					}
					else {
						FD_CLR(mClients.at(i)->GetSocket(), &mFdRead);
						mClients.erase(mClients.begin() + i);
						AddEvent(make_unique<NetworkEvent>(NetworkEvent::eClientDisconnected, nullptr, i));
						i--;
					}
				}
			}

			/* Handle server events. */
			if (mClientServer.IsConnected())
				if (FD_ISSET(mClientServer.GetSocket(), &fd_read)) {
					unique_ptr<NetworkEvent> e = make_unique<NetworkEvent>();
					int dataLen = ReceiveFromSocket(mClientServer, e.get());
					if (dataLen > 0) {
						AddEvent(move(e));
					}
					else {
						AddEvent(make_unique<NetworkEvent>(NetworkEvent::eServerDisconnected));
						mClientServer.Close();
					}
				}
		}
	});
}

void SServer::AddClient(shared_ptr<ClientSocket> client) {
	mClients.push_back(client);
}

size_t SServer::GetNumClients() {
	return mClients.size();
}

bool SServer::Connect(string ip, uint16_t port) {
	if (!mClientServer.Connect(ip, port))
		return false;
	FD_SET(mClientServer.GetSocket(), &mFdRead);
	StartNetworkThread();
	return true;
}

bool SServer::OpenServer(string ip, uint16_t port) {
	if (mServer.IsConnected())
		mServer.Close();
	mServer.SetMaxClients(4);

	if (!mServer.OpenServer("0.0.0.0", 1440))
		return false;
	FD_SET(mServer.GetSocket(), &mFdRead);
	StartNetworkThread();
	return true;
}

bool SServer::SendToSocket(ClientSocket &socket, NetworkEvent &e) {
	SerializedData data = e.Serialize();
	NetworkPacketHeader header(data.GetSize());
	SerializedData packet = header.Serialize();
	packet.Push(data);
	return socket.Send(packet.GetByteArray()->data(), packet.GetSize());
}

int SServer::ReceiveFromSocket(ClientSocket &socket, NetworkEvent *e) {
	NetworkPacketHeader header;
	uint8_t data[RECEIVE_BUFFER_SIZE];
	int recvLen = socket.Receive(&data, NETWORK_PACKET_HEADER_SIZE, NETWORK_PACKET_HEADER_SIZE);
	if (recvLen == NETWORK_PACKET_HEADER_SIZE) {
		SerializedData serializedHeader(data, NETWORK_PACKET_HEADER_SIZE);
		header.Deserialize(serializedHeader);
		recvLen = socket.Receive(&data, header.mSize, header.mSize);
		if (recvLen == header.mSize) {
			SerializedData serialized(data, header.mSize);
			e->Deserialize(serialized);
		}
	}
	return recvLen;
}

bool SServer::Send(NetworkEvent e) {
	return SendToSocket(mClientServer, e);
}

void SServer::SendTo(size_t id, NetworkEvent e) {
	SendToSocket(*mClients.at(id), e);
}

void SServer::SendToAll(NetworkEvent e, int excludedId /* = -1 */) {
	for (auto client : mClients) {
		if (excludedId < 0 || client != mClients.at(excludedId)) {
			SendToSocket(*client, e);
		}
	}
}

void SServer::AddEvent(unique_ptr<NetworkEvent> e) {
	lock_guard<mutex> lock(mEventQueueMutex);
	mEventQueue.push(unique_ptr<NetworkEvent>(move(e)));
}

unique_ptr<NetworkEvent> SServer::GetEvent() {
	lock_guard<mutex> lock(mEventQueueMutex);
	unique_ptr<NetworkEvent> e(move(mEventQueue.front()));
	mEventQueue.pop();
	return e;
}

bool SServer::IsEventAvailable() {
	lock_guard<mutex> lock(mEventQueueMutex);
	return (mEventQueue.size() > 0);
}

bool SServer::IsLocal() {
	return mServer.IsConnected();
}