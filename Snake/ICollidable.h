#pragma once

class Snake;
class Tile;
class FruitSpawner;

class ICollidable {
public:
	virtual ~ICollidable() = default;
	virtual void Collision(ICollidable &, int collisionX, int collisionY) {}
	virtual void Collision(Snake &, int collisionX, int collisionY) {}
	virtual void Collision(Tile &, int collisionX, int collisionY) {}
	virtual void Collision(FruitSpawner &, int collisionX, int collisionY) {}
};