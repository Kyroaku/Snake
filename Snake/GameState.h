#pragma once

#include "BaseState.h"
#include "InputController.h"
#include "Snake.h"
#include "FruitSpawner.h"
#include "Map.h"
#include "InputCommands.h"
#include "ConsoleRenderer.h"

class GameState : public BaseState {
	InputController mInputController;

	vector<shared_ptr<Snake>> mSnakes;
	shared_ptr<FruitSpawner> mFruitSpawner;
	Map mMap;

	ConsoleRenderer *mRenderer;

public:
	GameState();
	~GameState();

	void HandleInput(int key);
	void HandleNetwork(unique_ptr<NetworkEvent> e);
	void Update();
	void Draw();
};