#include "FruitSpawner.h"

#include "Snake.h"

#include "Server.h"

FruitSpawner::FruitSpawner(Map *map)
	: mMap(map) {
}

void FruitSpawner::Update() {
	if (SendRequest()) {
		if (Server.IsLocal())
			Server.SendToAll(NetworkEvent(NetworkEvent::eFruitsInfo, new Actor(*this)));
		SendRequest(false);
	}
}


void FruitSpawner::Collision(ICollidable &collider, int collisionX, int collisionY) {
	collider.Collision(*this, collisionX, collisionY);
}

void FruitSpawner::Collision(Snake &collider, int collisionX, int collisionY) {
	/* return if collision is not with snake's head. */
	Mesh &head = collider.GetMeshes().back();
	if (head.GetX() != collisionX || head.GetY() != collisionY)
		return;

	/* Find colliding fruit. */
	for (size_t i = 0; i < mFruits.size(); i++) {
		if (GetMeshes().at(i).GetX() != collisionX || GetMeshes().at(i).GetY() != collisionY)
			continue;

		/* Handle fruit collision. */
		mFruits.at(i)->OnEat(collider);
		RemoveFruit(i);

		SpawnFruit();
	}
}

void FruitSpawner::RemoveFruit(size_t i) {
	GetMeshes().erase(GetMeshes().begin() + i);
	mFruits.erase(mFruits.begin() + i);

	SendRequest(true);
}

void FruitSpawner::SpawnFruit() {
	unique_ptr<Fruit> fruit = make_unique<Fruit>();
	fruit->SetPosition(rand() % (mMap->GetWidth()-2) + 1, rand() % (mMap->GetHeight()-2) + 1);
	mFruits.push_back(move(fruit));
	GetMeshes().push_back(*mFruits.back());

	SendRequest(true);
}

void FruitSpawner::SendRequest(bool b) {
	mSendRequest = b;
}

bool FruitSpawner::SendRequest() {
	return mSendRequest;
}

//SerializedData FruitSpawner::Serialize() {
//	SerializedData data = __super::Serialize();
//	int len = mFruits.size();
//	data.Push(len);
//	for (auto &fruit : mFruits)
//		data.Push(fruit->Serialize());
//	return data;
//}
//
//void FruitSpawner::Deserialize(SerializedData &data) {
//	__super::Deserialize(data);
//	int len;
//	data.Pop(len);
//	mFruits.resize(len);
//	for (auto &fruit : mFruits)
//		fruit->Deserialize(data);
//}