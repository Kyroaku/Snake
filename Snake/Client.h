#pragma once

#include <string>
#include <memory>

#include "ClientSocket.h"

using std::string;
using std::shared_ptr;

class Client {
private:
	shared_ptr<ClientSocket> mSocket;
	string mName = "unknown";

public:
	Client(shared_ptr<ClientSocket> socket);

	shared_ptr<ClientSocket> GetSocket();
	SOCKET GetSocketHandle();
	string GetName();

	void SetName(string name);
};