#pragma once

#include <string>

using std::string;

class WindowOption {
public:
	enum EFlags {
		eSmall = 0x01,
		eNotSelectable = 0x02
	};

public:
	string mText = "";
	int mId = 0;
	uint8_t mFlags;

	WindowOption(string text = "", int id = 0, uint8_t flags = 0)
		: mText(text)
		, mId(id)
		, mFlags(flags) {}

	bool HasFlagSet(EFlags flag);
};