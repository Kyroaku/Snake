#include "InputController.h"

void InputController::SetLeftKeyCommand(shared_ptr<InputCommand> command) {
	mLeftKey = command;
}

void InputController::SetRightKeyCommand(shared_ptr<InputCommand> command) {
	mRightKey = command;
}
void InputController::SetUpKeyCommand(shared_ptr<InputCommand> command) {
	mUpKey = command;
}

void InputController::SetDownKeyCommand(shared_ptr<InputCommand> command) {
	mDownKey = command;
}

void InputController::SetConfirmKeyCommand(shared_ptr<InputCommand> command) {
	mConfirmKey = command;
}


std::shared_ptr<InputCommand> InputController::HandleInput(int key) {
	switch (key) {
	case 19424: case 'a': return mLeftKey;
	case 19936: case 'd': return mRightKey;
	case 18656: case 'w': return mUpKey;
	case 20704: case 's': return mDownKey;
	case 13: return mConfirmKey;
	}
	return 0;
}