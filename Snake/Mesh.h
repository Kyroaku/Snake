#pragma once

#include "Pixel.h"
#include "ISerializable.h"

class Mesh : public ISerializable {
private:
	Pixel mPixel;
	int mPosX, mPosY;

public:
	Mesh();

	Pixel &GetPixel();
	int GetX();
	int GetY();

	void SetPosition(int x, int y);

	SerializedData Serialize();
	void Deserialize(SerializedData &data);
};