#pragma once

#include <memory>

#include "Command.h"

using namespace std;

class InputController {
	shared_ptr<InputCommand> mLeftKey;
	shared_ptr<InputCommand> mRightKey;
	shared_ptr<InputCommand> mUpKey;
	shared_ptr<InputCommand> mDownKey;
	shared_ptr<InputCommand> mConfirmKey;

public:
	void SetLeftKeyCommand(shared_ptr<InputCommand> command);
	void SetRightKeyCommand(shared_ptr<InputCommand> command);
	void SetUpKeyCommand(shared_ptr<InputCommand> command);
	void SetDownKeyCommand(shared_ptr<InputCommand> command);
	void SetConfirmKeyCommand(shared_ptr<InputCommand> command);

	shared_ptr<InputCommand> HandleInput(int key);
};