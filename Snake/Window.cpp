#include "Window.h"

#include "Console.h"

Window::Window(string title /* = "" */)
	: mTitle(title) {

}

void Window::Draw() {
	Console.SetColor(Pixel::eWhite);
	Console.PrintCenter(ASCII::LeftTopCorner2 + string(26, ASCII::HorizontalLine2) + ASCII::RightTopCorner2, 0);
	Console.PrintCenter(mTitle, 1);
	Console.PrintCenter(ASCII::LeftBottomCorner2 + string(26, ASCII::HorizontalLine2) + ASCII::RightBottomCorner2, 2);

	short pos = 3;
	for (size_t i = 0; i < GetOptions().size(); i++) {
		if (mSelectedOption == i)
			Console.SetColor(Pixel::eWhite);
		else
			Console.SetColor(Pixel::eGray);
		if (mOptions.at(i).HasFlagSet(WindowOption::eSmall)) {
			Console.PrintCenter(mOptions.at(i).mText, pos);
			pos++;
		}
		else {
			Console.PrintCenter(ASCII::LeftTopCorner + string(20, ASCII::HorizontalLine) + ASCII::RightTopCorner, pos);
			Console.PrintCenter(mOptions.at(i).mText, pos + 1);
			Console.PrintCenter(ASCII::LeftBottomCorner + string(20, ASCII::HorizontalLine) + ASCII::RightBottomCorner, pos + 2);
			pos += 3;
		}
	}
}

void Window::AddOption(WindowOption option) {
	mOptions.push_back(option);
}

void Window::SetSelectedOption(int8_t i) {
	mSelectedOption = i - 1;
	SelectNext();
}

void Window::SetSelectedOptionById(int8_t id) {
	for (size_t i = 0; i < mOptions.size(); i++) {
		if (mOptions.at(i).mId == id && !mOptions.at(i).HasFlagSet(WindowOption::eNotSelectable))
			mSelectedOption = (int8_t)i;
	}
}

void Window::SetTitle(string title) {
	mTitle = title;
}

void Window::SetWindowConfirmListener(IWindowConfirmListener *listener) {
	mWindowConfirmListener = listener;
}

WindowOption *Window::GetSelectedOption() {
	if (mSelectedOption >= 0 && mSelectedOption < (int8_t)mOptions.size())
		return &mOptions.at(mSelectedOption);
	else
		return 0;
}

string Window::GetTitle() {
	return mTitle;
}

vector<WindowOption> &Window::GetOptions() {
	return mOptions;
}

void Window::RemoveAllOptions() {
	mOptions.clear();
}

void Window::SelectNext() {
	size_t size = mOptions.size();
	for (size_t j = 0; j < size; j++) {
		int index = (mSelectedOption + 1 + j) % size;
		if (!mOptions.at(index).HasFlagSet(WindowOption::eNotSelectable)) {
			mSelectedOption = index;
			return;
		}
	}
	mSelectedOption = -1;
}

void Window::SelectPrevious() {
	size_t size = mOptions.size();
	for (size_t j = 0; j < size; j++) {
		int index = (size + mSelectedOption - 1 - j) % size;
		if (!mOptions.at(index).HasFlagSet(WindowOption::eNotSelectable)) {
			mSelectedOption = index;
			return;
		}
	}
	mSelectedOption = -1;
}

void Window::Confirm() {
	if (mWindowConfirmListener && mSelectedOption >= 0)
		mWindowConfirmListener->OnWindowConfirm(this, mOptions.at(mSelectedOption).mId);
}