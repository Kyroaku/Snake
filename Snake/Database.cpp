#include "Database.h"

SDatabase::SDatabase() {

}

SDatabase::~SDatabase() {

}

SDatabase &SDatabase::GetInstance() {
	static SDatabase instance;
	return instance;
}

void SDatabase::Clear() {
	mPlayers.clear();
}

void SDatabase::AddPlayer(PlayerData *player) {
	mPlayers.push_back(shared_ptr<PlayerData>(player));
}

void SDatabase::RemovePlayer(size_t i) {
	mPlayers.erase(mPlayers.begin() + i);
}

shared_ptr<PlayerData> SDatabase::GetPlayer(size_t id) {
	return mPlayers.at(id);
}

vector<shared_ptr<PlayerData>> &SDatabase::GetPlayers() {
	return mPlayers;
}