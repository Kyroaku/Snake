#pragma once

#include <WinSock2.h>
#include <WS2tcpip.h>

#include "ClientSocket.h"
#include "ServerSocket.h"

#pragma comment(lib, "ws2_32")

#define Network (NetworkManager::GetInstance())

class NetworkManager {
private:
	NetworkManager();
	NetworkManager(const NetworkManager&) = delete;
	void operator=(const NetworkManager&) = delete;
public:
	~NetworkManager();

	static NetworkManager &GetInstance();

	bool Init();
};