#pragma once

#include "Command.h"

/* ======================================== */
/* ============ Local commands ============ */
/* ======================================== */

class LeftCommand : public InputCommand {
public:
	void Execute(void *param);
};

class RightCommand : public InputCommand {
public:
	void Execute(void *param);
};

class UpCommand : public InputCommand {
public:
	void Execute(void *param);
};

class DownCommand : public InputCommand {
public:
	void Execute(void *param);
};

/* ======================================== */
/* =========== Network commands =========== */
/* ======================================== */

class ClientLeftCommand : public InputCommand {
public:
	void Execute(Snake &snake);
};

class ClientRightCommand : public InputCommand {
public:
	void Execute(Snake &snake);
};

class ClientUpCommand : public InputCommand {
public:
	void Execute(Snake &snake);
};

class ClientDownCommand : public InputCommand {
public:
	void Execute(Snake &snake);
};