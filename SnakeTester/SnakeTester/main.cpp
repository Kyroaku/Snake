#include <conio.h>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <future>

#include "../../Snake/NetworkManager.h"

using namespace std;

int main() {
	Network.Init();

	ServerSocket server;
	vector<shared_ptr<ClientSocket>> clients;

	fd_set fdRead;
	FD_ZERO(&fdRead);

	future<void> f;

	f = async(launch::async, [&]() {
		while (1) {
			fd_set fd_read = fdRead;
			int result = select(0, &fd_read, 0, 0, 0);
			if (result <= 0) {
				this_thread::sleep_for(10ms);
				continue;
			}

			if (FD_ISSET(server.GetSocket(), &fd_read)) {
				shared_ptr<ClientSocket> client = shared_ptr<ClientSocket>(server.Accept());
				if (client->IsConnected())
					clients.push_back(client);
				FD_SET(client->GetSocket(), &fdRead);
				cout << client->GetIp() << ":" << client->GetPort() << " connected" << endl;
			}
			for (size_t i = 0; i < clients.size(); i++) {
				if (FD_ISSET(clients.at(i)->GetSocket(), &fd_read)) {
					uint8_t command = 0;
					if (clients.at(i)->Receive(&command, sizeof(command)))
						cout << clients.at(i)->GetIp() << ":" << clients.at(i)->GetPort() << ": " << (int)command << endl;
					else {
						cout << clients.at(i)->GetIp() << ":" << clients.at(i)->GetPort() << " disconnected" << endl;
						FD_CLR(clients.at(i)->GetSocket(), &fdRead);
						clients.erase(clients.begin() + i);
						i--;
					}
				}
			}
		}
	});
	
	server.SetMaxClients(10);
	server.OpenServer("0.0.0.0", 1440);
	FD_SET(server.GetSocket(), &fdRead);

	cout << "Waiting for client..." << endl;

	return 0;
}